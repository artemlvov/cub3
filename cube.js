﻿(function($){

	var wCub3 = function( $node, params ){

		var me = this
		,	$globalEventHandler = $( document.body )
		,	$cube = $( params.rotatorSelector, $node )
		,	isMouseTracked = false
		,	trackDiscreteTimer = null
		,	nodeOffset = $node.offset()
		,	nodeSize = [$node.width(), $node.height()]
		,	curMousePos = []
		// ,	curMatrix = {
		// 		[1,0,0,0],
		// 		[0,1,0,0],
		// 		[0,0,1,0],
		// 		[0,0,0,1]
		// 	}
		;

		function _getAngleByPoints( m1, m2 ){
			var alphaRad = 0;
			if ( m1 * m2 < 0 ) {
				alphaRad = Math.PI - ( Math.acos( 2 * Math.abs( m1 ) / nodeSize[0] ) + Math.acos( 2 * Math.abs( m2 ) / nodeSize[0] ) );
				alphaRad = m1 > m2 ? -alphaRad : alphaRad;
			} else {
				alphaRad = Math.acos( 2 * m1 / nodeSize[0] ) - Math.acos( 2 * m2 / nodeSize[0] );
			}
			// var h1 = Math.sqrt( m1 * ( 2 * params.radius - m1 ) )
			// ,	h2 = Math.sqrt( m2 * ( 2 * params.radius - m2 ) )
			// ,	c = Math.sqrt( Math.pow( ( h1 - h2 ), 2 ) + Math.pow( ( m2 - m1 ), 2 ) )
			// ,	alphaRad = Math.acos( c / ( 2 * params.radius ) )
			// ;
			return 180 * alphaRad / Math.PI;
		};

		function _rotateCube( coords ){
			coords = $.extend( { x: 0, y: 0, z: 0 }, coords );
			console.log(coords);
			$cube.css( { 'transform': 'rotateX(' + coords.x + 'deg) rotateY(' + coords.y + 'deg) rotateZ(' + coords.z + 'deg)' } );
		};

		function _startTracking( e ){
			isMouseTracked = true;
			curMousePos = _normalizePoint( [e.clientX, e.clientY] );
		};

		function _stopTracking( e ){
			e.stopPropagation();
			isMouseTracked = false;
			curMousePos = [];
			clearTimeout( trackDiscreteTimer ); trackDiscreteTimer = null;
		};

		function _normalizePoint( point ){
			return [
				point[0] - nodeOffset.left - nodeSize[0] / 2,
				point[1] - nodeOffset.top - nodeSize[1] / 2
			]
		}

		function _handleMouseMove( e ){
			if ( isMouseTracked ) {
				if ( !trackDiscreteTimer ) {
					trackDiscreteTimer = setTimeout( function(){
						clearTimeout( trackDiscreteTimer ); trackDiscreteTimer = null;
					}, params.trackInterval );
					var normalizedPoint = _normalizePoint( [e.clientX, e.clientY] );
					console.log(curMousePos);
					console.log(normalizedPoint);
					_rotateCube( {
						y: _getAngleByPoints( curMousePos[0], normalizedPoint[0] )
					// 	x: _getAngleByPoints( curMousePos[1], normalizedPoint[1] )
					} );
					curMousePos = normalizedPoint;
				}				
			}
		}

		function _events(){
			$node
				.bind( 'mousedown', _startTracking )
				.bind( 'mouseup', _handleMouseMove )
			;

			// $globalEventHandler
			// 	.bind( 'mouseup', _stopTracking )
			// 	.bind( 'mousemove', _handleMouseMove )
			// ;
		}

		var init = function(){

			_events();

		}();

	};

	var defaultConfigCub3 = {
		radius: 400,
		rotatorSelector: '.cube',
		trackInterval: 1000
	};

	$.fn.cub3 = function( params ){
		if ( typeof( params ) === 'object' || typeof( params ) === 'undefined' ) {
			params = $.extend( {}, defaultConfigCub3, params );
		}
		return this.each( function(){
			var $this = $( this )
			,	curInstance = $this.data( 'cub3' );
			if ( curInstance ) {
				if ( typeof( params ) === 'string' && curInstance[ params ] ) {
					curInstance[ params ]();
				}
			} else {
				$this.data( 'cub3', new wCub3( $this, params ) );
			}
		} );
	};

	$( function(){
    
		$( '.cube-container' ).cub3();
    
	} );
})(jQuery)